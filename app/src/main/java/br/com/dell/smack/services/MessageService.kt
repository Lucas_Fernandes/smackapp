package br.com.dell.smack.services

import br.com.dell.smack.model.Channel
import br.com.dell.smack.model.Message

object MessageService {
    var channels = ArrayList<Channel>()
    var messages = ArrayList<Message>()

    fun clearChannels() { channels.clear() }
    fun clearMessages() { messages.clear() }
}