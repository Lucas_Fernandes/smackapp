package br.com.dell.smack.network.model.request

data class LoginModelRequest(val email: String, val password: String)
