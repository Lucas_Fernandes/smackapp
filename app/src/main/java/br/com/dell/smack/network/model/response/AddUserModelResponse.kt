package br.com.dell.smack.network.model.response

data class AddUserModelResponse(val __v: String,
                                val avatarColor: String,
                                val avatarName: String,
                                val email:String,
                                val name:String,
                                val _id:String)
