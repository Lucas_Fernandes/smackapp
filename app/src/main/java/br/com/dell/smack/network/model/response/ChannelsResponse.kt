package br.com.dell.smack.network.model.response

data class ChannelsResponse(
    val _id: String,
    val name: String,
    val description: String,
    val __v: Int
)
