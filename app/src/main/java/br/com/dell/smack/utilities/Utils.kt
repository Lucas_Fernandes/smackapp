package br.com.dell.smack.utilities

import android.content.Context
import android.graphics.Color
import android.view.inputmethod.InputMethodManager
import java.util.*


object Utils {

    fun returnAvatarColor(components: String): Int {
        val rgbColorsArray = IntArray(3)

        val strippedColor = components.replace("{", "")
                                             .replace("}", "")
                                             .replace("[", "")
                                             .replace("]", "")
                                             .replace(",", "")

        Scanner(strippedColor).let {
            if(it.hasNext()){
                rgbColorsArray[0] = (it.nextDouble() * 255).toInt()
                rgbColorsArray[1] = (it.nextDouble() * 255).toInt()
                rgbColorsArray[2] = (it.nextDouble() * 255).toInt()
            }
        }
        return Color.rgb(rgbColorsArray[0], rgbColorsArray[1], rgbColorsArray[2])
    }

}

