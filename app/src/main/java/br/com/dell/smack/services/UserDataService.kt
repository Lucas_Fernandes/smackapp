package br.com.dell.smack.services

import br.com.dell.smack.controller.App

object UserDataService {

    var id: String = ""
    var userName = ""
    var userAvatarName = ""
    var userAvatarColor = ""

    fun logout() {
        App.prefs!!.apply {
            id = ""
            userName = ""
            userAvatarName = ""
            userAvatarColor = ""
            isLoggedIn = false
            userEmail = ""
            authToken = ""
            MessageService.clearMessages()
            MessageService.clearChannels()
        }
    }

}