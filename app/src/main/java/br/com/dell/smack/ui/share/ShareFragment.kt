package br.com.dell.smack.ui.share

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import br.com.dell.smack.R
import kotlinx.android.synthetic.main.fragment_share.*
import kotlinx.android.synthetic.main.nav_header_main.*

class ShareFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_share, container, false)
}