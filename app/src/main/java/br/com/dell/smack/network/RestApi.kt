package br.com.dell.smack.network

import br.com.dell.smack.network.model.request.AddUserModelRequest
import br.com.dell.smack.network.model.request.LoginModelRequest
import br.com.dell.smack.network.model.request.RegisterModelRequest
import br.com.dell.smack.network.model.response.*
import retrofit2.Call
import retrofit2.http.*

interface RestApi {

    @POST("account/register")
    fun register(@Body registerModel: RegisterModelRequest): Call<String>

    @POST("account/login")
    fun login(@Body loginModel: LoginModelRequest): Call<LoginModelResponse>

    @POST("user/add")
    fun createUser(@Header("Authorization") authToken:String,
                   @Body addUserModel: AddUserModelRequest): Call<AddUserModelResponse>

    @GET("user/byEmail/{email}")
    fun getUser(@Header("Authorization") authToken: String,
                @Path("email") email:String): Call<FindUserModelResponse>

    @GET("/v1/channel")
    fun getChannels(@Header("Authorization") authToken: String): Call<List<ChannelsResponse>>

    @GET("/v1/message/byChannel/{channelId}")
    fun getMessagesFromChannel(@Header("Authorization") authToken: String,
                               @Path("channelId") channelId: String): Call<List<MessageFromChannelModelResponse>>
}