package br.com.dell.smack.controller

import android.app.Application
import br.com.dell.smack.utilities.SharedPrefs

class App : Application() {

    companion object {
        var prefs: SharedPrefs? = null

    }

    override fun onCreate() {
        prefs = SharedPrefs(applicationContext)
        super.onCreate()
    }

}