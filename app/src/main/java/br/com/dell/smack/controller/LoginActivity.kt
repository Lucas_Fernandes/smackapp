package br.com.dell.smack.controller

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import br.com.dell.smack.R
import br.com.dell.smack.services.RetrofitRequests
import br.com.dell.smack.utilities.LOGIN_DATA
import kotlinx.android.synthetic.main.activity_login.*



class LoginActivity : AppCompatActivity() {

    private val CHANNEL_DOWLOAD_TAG = "CHANNEL_DOWNLOAD"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        loginProgressBar.visibility = View.INVISIBLE

        loginButton.setOnClickListener { loginLoginButtonClicked() }
        createAccountButton.setOnClickListener { createUserBtnClicked() }
    }

    private fun createUserBtnClicked() {
        startActivity(Intent(this@LoginActivity, CreateUserActivity::class.java))
    }

    private fun loginLoginButtonClicked() {
        hideKeyboard()
        enableSpinner(true)
        val email = loginEmailText.text.toString()
        val password = loginPasswordText.text.toString()

        when {
            email.isEmpty() -> {
                emailPasswordErrorToast(getString(R.string.please_inform_email))
                return
            }

            password.isEmpty() -> {
                emailPasswordErrorToast(getString(R.string.please_inform_password))
                return
            }

            else -> { callLoginService(email, password) }
        }

    }

    private fun emailPasswordErrorToast(msg: String) {
        enableSpinner(false)
        Toast.makeText(this@LoginActivity,msg , Toast.LENGTH_SHORT).show()
    }

    private fun callLoginService(email: String, password: String) {
        if(!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            enableSpinner(false)
            emailPasswordErrorToast(getString(R.string.invalid_email))
            return
        }

        RetrofitRequests.loginUser(email, password,
            this@LoginActivity) { message: String, loginSuccess: Boolean ->
            if (loginSuccess) {
                callFindUserByEmailService(email)
            } else {
                enableSpinner(false)
                Toast.makeText(this@LoginActivity, message, Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun downloadChannels() {
        RetrofitRequests.getChannels(this@LoginActivity) { message, requestSuccess ->
            if(requestSuccess){
                val intent = Intent(this@LoginActivity, MainActivity::class.java).apply {
                    this.putExtra(LOGIN_DATA, LOGIN_DATA)
                }
                startActivity(intent)
            } else {
                enableSpinner(false)
                Toast.makeText(this@LoginActivity, message, Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun callFindUserByEmailService(email: String) {
        RetrofitRequests.findUserByEmail(this@LoginActivity, email) { message, requestSuccess ->
            if(requestSuccess){
                enableSpinner(false)
                downloadChannels()
            } else {
                enableSpinner(false)
                Toast.makeText(this@LoginActivity, message, Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun hideKeyboard() {
        val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.let {
            if(it.isAcceptingText){
                it.hideSoftInputFromWindow(currentFocus?.windowToken, 0)
            }
        }
    }


    private fun enableSpinner (enable: Boolean) {
        if (enable) {
            loginProgressBar.visibility = View.VISIBLE
        } else {
            loginProgressBar.visibility = View.INVISIBLE
        }
    }

}
