package br.com.dell.smack.network.model.request

data class RegisterModelRequest(val email: String, val password: String)