package br.com.dell.smack.network.model.request

data class FindUserModelRequest(val email: String)