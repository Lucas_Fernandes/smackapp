package br.com.dell.smack.services

import android.content.Context
import br.com.dell.smack.R
import br.com.dell.smack.controller.App
import br.com.dell.smack.model.Channel
import br.com.dell.smack.model.Message
import br.com.dell.smack.network.RetrofitClient
import br.com.dell.smack.network.model.request.AddUserModelRequest
import br.com.dell.smack.network.model.request.LoginModelRequest
import br.com.dell.smack.network.model.request.RegisterModelRequest
import br.com.dell.smack.network.model.response.*
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

private val restApi by lazy { RetrofitClient.retrofit }

object RetrofitRequests {

    fun registerUser(email: String, password: String, context: Context,
                     responseMessage: (message: String, requestSuccess: Boolean) -> (Unit)) {

        restApi.register(RegisterModelRequest(email, password)).enqueue(object : Callback<String> {
            override fun onResponse(call: Call<String>, response: Response<String>) {
                if (!response.isSuccessful) {
                    responseMessage(context.getString(R.string.registration_error_message), false)
                } else {
                    responseMessage(context.getString(R.string.registration_success_message), true)
                }
            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                responseMessage("Error ${t.message.toString()}", false)
            }
        })
    }

    fun loginUser(email: String, password: String, context: Context,
        responseMessage: (message: String, requestSuccess: Boolean) -> Unit) {

        restApi.login(LoginModelRequest(email, password))
            .enqueue(object : Callback<LoginModelResponse> {
                override fun onResponse(call: Call<LoginModelResponse>, response: Response<LoginModelResponse>) {
                    if (!response.isSuccessful) {
                        checkLoginErrorResponse(response, responseMessage, context)
                    } else {
                        response.body().let {
                            if (it != null) {
                                App.prefs!!.apply {
                                    authToken = it.token
                                    userEmail = it.user
                                    isLoggedIn = true
                                }
                                responseMessage(context.getString(R.string.welcome_message), true)
                            }
                        }
                    }
                }

                override fun onFailure(call: Call<LoginModelResponse>, t: Throwable) {
                    responseMessage("Error ${t.message.toString()}", false)
                }

            })
    }

    private fun checkLoginErrorResponse(response: Response<LoginModelResponse>,
                                        responseMessage: (String, Boolean) -> Unit,
                                        context: Context) {
        if (response.code() == 401) {
            responseMessage(context.getString(R.string.login_email_password_error_message), false)
        } else {
            responseMessage(context.getString(R.string.login_general_error_message), false)
        }
    }


    fun createUser(name: String, email: String,
                   avatarName: String, avatarColor: String, context: Context,
                   responseMessage: (message: String, requestSuccess: Boolean) -> Unit) {

        restApi.createUser("Bearer ${App.prefs!!.authToken}",
                            AddUserModelRequest(name, email, avatarName, avatarColor)).enqueue(object : Callback<AddUserModelResponse> {

            override fun onResponse(call: Call<AddUserModelResponse>, response: Response<AddUserModelResponse>) {
                if (!response.isSuccessful) {
                    responseMessage(context.getString(R.string.user_creation_error_message), false)
                } else {
                    responseMessage(context.getString(R.string.user_creation_success_message), true)
                    response.body().let {
                        if (it != null) {
                            UserDataService.userName = it.name
                            UserDataService.userAvatarName = it.avatarName
                            UserDataService.userAvatarColor = it.avatarColor
                        }
                    }
                }
            }

            override fun onFailure(call: Call<AddUserModelResponse>, t: Throwable) {
                responseMessage("Error ${t.message.toString()}", false)
            }
        })
    }

    fun findUserByEmail(context: Context, email: String, responseMessage: (message:String, requestSuccess:Boolean) -> Unit) {
        restApi.getUser("Bearer ${App.prefs!!.authToken}", email)
               .enqueue(object : Callback<FindUserModelResponse>{

            override fun onResponse(call: Call<FindUserModelResponse>, response: Response<FindUserModelResponse>) {
                if(response.isSuccessful){
                   response.body().let {
                       if(it != null){
                           UserDataService.userName = it.name
                           UserDataService.userAvatarName = it.avatarName
                           UserDataService.userAvatarColor = it.avatarColor
                           UserDataService.id = it._id
                       }
                   }
                    responseMessage(context.getString(R.string.welcome_message), true)
                }
            }

            override fun onFailure(call: Call<FindUserModelResponse>, t: Throwable) {
                responseMessage("Error ${t.localizedMessage}", false)
            }
        })
    }

    fun getChannels(context: Context, responseMessage: (message: String, requestSuccess: Boolean) -> Unit) {
        restApi.getChannels("Bearer ${App.prefs!!.authToken}")
               .enqueue(object : Callback<List<ChannelsResponse>>{

                   override fun onResponse(call: Call<List<ChannelsResponse>>, response: Response<List<ChannelsResponse>>) {
                       response.let{
                           if(it.isSuccessful){
                               val gson = Gson()
                               val responseList = gson.fromJson(gson.toJson(it.body()),
                                                                  Array<ChannelsResponse>::class.java).toList()
                                for(x in responseList.indices) {
                                    val channel = Channel(id = responseList[x]._id,
                                                          name = responseList[x].name,
                                                          description = responseList[x].description)

                                    MessageService.channels.add(channel)
                                }

                               responseMessage(context.getString(R.string.channels_downloaded_successfully), true)
                           } else {
                               responseMessage(context.getString(R.string.error_download_channels), false)
                           }
                       }
                   }

                   override fun onFailure(call: Call<List<ChannelsResponse>>, t: Throwable) {
                       responseMessage("Error ${t.localizedMessage}", false)
                   }
               })
    }

    fun getMessagesFromChannel(context: Context, channelId: String,
                               responseMessage: (message: String, requestSuccess: Boolean) -> Unit) {
        restApi.getMessagesFromChannel("Bearer ${App.prefs!!.authToken}", channelId)
               .enqueue(object : Callback<List<MessageFromChannelModelResponse>> {

                   override fun onResponse(call: Call<List<MessageFromChannelModelResponse>>,
                                           response: Response<List<MessageFromChannelModelResponse>>) {
                       response.let {
                           if(it.isSuccessful) {
                               MessageService.clearMessages()

                               val gson = Gson()
                               val responseList = gson.fromJson(gson.toJson(it.body()),
                                                                Array<MessageFromChannelModelResponse>::class.java).toList()

                               for(x in responseList.indices) {
                                   val message = Message(id = responseList[x]._id, userName = responseList[x].userName,
                                                         channelId = responseList[x].channelId, message = responseList[x].messageBody,
                                                         userAvatar = responseList[x].userAvatar, userAvatarColor = responseList[x].userAvatarColor,
                                                         timeStamp = responseList[x].timeStamp)

                                   MessageService.messages.add(message)
                               }

                               responseMessage(context.getString(R.string.message_downloaded_sucessfully), true)
                           } else {
                               responseMessage(context.getString(R.string.error_downloading_messages), false)
                           }
                       }
                   }

                   override fun onFailure(call: Call<List<MessageFromChannelModelResponse>>, t: Throwable) {
                       responseMessage("Error ${t.localizedMessage}", false)
                   }

               })
    }


}