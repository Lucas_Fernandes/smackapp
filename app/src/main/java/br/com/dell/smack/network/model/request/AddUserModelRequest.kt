package br.com.dell.smack.network.model.request

data class AddUserModelRequest (val name:String,
                                val email:String,
                                val avatarName:String,
                                val avatarColor: String)
