package br.com.dell.smack.network.model.response

data class LoginModelResponse(val user: String, val token: String)