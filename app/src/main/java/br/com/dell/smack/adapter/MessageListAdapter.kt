package br.com.dell.smack.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.dell.smack.R
import br.com.dell.smack.model.Message
import br.com.dell.smack.utilities.Utils
import kotlinx.android.synthetic.main.message_list_view.view.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class MessageListAdapter(
    private val context: Context,
    private val layoutInflater: LayoutInflater,
    private val messageList: ArrayList<Message>
) : RecyclerView.Adapter<MessageListAdapter.ViewHolder>() {

    private val parseTag = "PARSE"

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(layoutInflater.inflate(R.layout.message_list_view, parent, false))

    override fun getItemCount() = messageList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(position)

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(position: Int) {
            itemView.apply {
                setImageBackground(position, this)
                tvMessageUserName.text = messageList[position].userName
                tvMessageTimeStamp.text = returnDateString(messageList[position].timeStamp)
                tvMessageBody.text = messageList[position].message
            }
        }

        private fun setImageBackground(position: Int, view: View) {
            val resourceId = context.resources.getIdentifier(
                messageList[position].userAvatar,
                "drawable",
                context.packageName
            )

            view.ivMessageUserImage.apply {
                setImageResource(resourceId)
                setBackgroundColor(Utils.returnAvatarColor(messageList[position].userAvatarColor))
            }
        }

        private fun returnDateString(string: String): String {
            var convertedDate: Date? = null
            val isoFormatter = SimpleDateFormat("dd-MM-yyyy'T'HH:mm:ss.SSS'Z'", Locale.getDefault())
            try {
                 convertedDate = isoFormatter.apply { timeZone = TimeZone.getTimeZone("UTC") }.parse(string)
            } catch (e: ParseException) {
            }

            return SimpleDateFormat("EEE, d MMM, ''yy" , Locale.getDefault()).format(convertedDate)
        }

    }
}