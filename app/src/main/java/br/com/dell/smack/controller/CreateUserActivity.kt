package br.com.dell.smack.controller

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import br.com.dell.smack.R
import br.com.dell.smack.services.RetrofitRequests
import kotlinx.android.synthetic.main.activity_create_user.*
import java.util.*

class CreateUserActivity : AppCompatActivity() {

    private var userAvatar = "profileDefault"
    private var avatarColor = "[0.5, 0.5, 0.5, 1]"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_user)

        setClickListenersToButtons()

        createUserPinner.visibility = View.INVISIBLE
    }

    private fun setClickListenersToButtons() {
        createAvatarImageView.setOnClickListener { generateUserAvatar() }
        createBackgorundColorBtn.setOnClickListener { generateColorClicked() }
        createUserButton.setOnClickListener { createUserClicked() }
    }

    private fun generateUserAvatar() {
        val binaryRandom = Random().nextInt(2)
        val avatarNumber = Random().nextInt(28)

        userAvatar = if (binaryRandom == 0) {
            "light$avatarNumber"
        } else {
            "dark$avatarNumber"
        }

        createAvatarImageView.setImageResource(
            resources.getIdentifier(
                userAvatar,
                "drawable",
                packageName
            )
        )
    }

    private fun createUserClicked() {
        val userName = createUserNameText.text.toString()
        val email = createEmailText.text.toString()
        val password = createPasswordText.text.toString()

        when {
            email.isEmpty() -> {
                showResponseMessage(getString(R.string.invalid_email_message))
                return
            }

            password.isEmpty() -> {
                showResponseMessage(getString(R.string.inform_password_message))
                return
            }

            else -> {
                callRegisterService(email, password, userName)
            }
        }
    }

    private fun callRegisterService(email: String, password: String, userName: String) {
        enableSpinner(true)
        RetrofitRequests.registerUser(email, password, this@CreateUserActivity) { message: String, registrationSuccess: Boolean ->
            if (registrationSuccess) {
                showResponseMessage(message)
                callLoginService(email, password, userName)
            } else {
                showResponseMessage(message)
            }
        }
    }

    private fun callLoginService(email: String, password: String, userName: String) {
        RetrofitRequests.loginUser(email, password,
            this@CreateUserActivity) { message: String, loginSuccess: Boolean ->
            if (loginSuccess) {
                callCreateUserService(userName, email)
                finish()
            } else {
                showResponseMessage(message)
            }
        }
    }

    private fun callCreateUserService(userName: String, email: String) {
        RetrofitRequests.createUser(userName, email,
                                    userAvatar, avatarColor,
                            this@CreateUserActivity) { message: String, createUserSuccess: Boolean ->
            if (createUserSuccess) {
                showResponseMessage(message)
                enableSpinner(false)
                val intent = Intent(this@CreateUserActivity, MainActivity::class.java).apply {
                    this.putExtra("LoginData", "LoginData")
                }
                startActivity(intent)
            } else {
                showResponseMessage(message)
            }
        }
    }

    private fun generateColorClicked() {
        val colorNumbers = arrayOf(
            Random().nextInt(255),
            Random().nextInt(255),
            Random().nextInt(255)
        )

        createAvatarImageView.setBackgroundColor(
            Color.rgb(
                colorNumbers[0],
                colorNumbers[1],
                colorNumbers[2]
            )
        )

        avatarColor = "{${colorNumbers[0].toDouble() / 255}, ${colorNumbers[1].toDouble() / 255}," +
                " ${colorNumbers[2].toDouble() / 255}, 1}"
    }


    private fun showResponseMessage(message: String) {
        Toast.makeText(this@CreateUserActivity, message, Toast.LENGTH_SHORT).show()
    }


    private fun enableSpinner (enable: Boolean) {
        if (enable) {
            createUserPinner.visibility = View.VISIBLE
        } else {
            createUserPinner.visibility = View.INVISIBLE
        }

        createUserButton.isEnabled = !enable
        createAvatarImageView.isEnabled = !enable
        createBackgorundColorBtn.isEnabled = !enable
    }
}