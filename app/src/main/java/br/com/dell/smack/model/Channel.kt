package br.com.dell.smack.model

data class Channel(val name:String, val description:String, val id:String)