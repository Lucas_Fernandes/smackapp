package br.com.dell.smack.utilities

import android.content.Context

class SharedPrefs(context: Context) {

    private val sharedPreferences = context.getSharedPreferences("prefs", 0)

    var isLoggedIn: Boolean
        get() = sharedPreferences.getBoolean("isLoggedIn", false)
        set(value) = sharedPreferences.edit().putBoolean("isLoggedIn", value).apply()

    var authToken: String?
        get() = sharedPreferences.getString("authToken", "")
        set(value) = sharedPreferences.edit().putString("authToken", value).apply()

    var userEmail: String?
        get() = sharedPreferences.getString("userEmail", "")
        set(value) = sharedPreferences.edit().putString("userEmail", value).apply()


}