package br.com.dell.smack.network.model.response

data class FindUserModelResponse(
    val _id: String,
    val avatarColor: String,
    val avatarName: String,
    val email: String,
    val name: String,
    val __v: String
)