package br.com.dell.smack.utilities

const val LOGIN_DATA = "LoginData"
const val BASE_URL = "https://smack-chat-app1.herokuapp.com/v1/"
const val SOCKET_URL = "https://smack-chat-app1.herokuapp.com"
