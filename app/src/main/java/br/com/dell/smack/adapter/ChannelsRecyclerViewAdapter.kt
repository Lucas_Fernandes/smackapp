package br.com.dell.smack.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.dell.smack.R
import br.com.dell.smack.model.Channel
import kotlinx.android.synthetic.main.channel_list_item.view.*

class ChannelsRecyclerViewAdapter(private val list: ArrayList<Channel>,
                                  private val layoutInflater: LayoutInflater,
                                  private val callback: (Int) -> Unit) : RecyclerView.Adapter<ChannelsRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
       ViewHolder(layoutInflater.inflate(R.layout.channel_list_item, parent, false))

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(position)

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        fun bind(position: Int) {
            itemView.apply {
                val channelName = "#${list[position].name}"
                setOnClickListener { callback(position) }
                channel_name_list_item.text = channelName
            }
        }
    }
}