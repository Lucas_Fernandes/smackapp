package br.com.dell.smack.controller

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.dell.smack.adapter.ChannelsRecyclerViewAdapter
import br.com.dell.smack.R
import br.com.dell.smack.adapter.MessageListAdapter
import br.com.dell.smack.model.Channel
import br.com.dell.smack.model.Message
import br.com.dell.smack.services.MessageService
import br.com.dell.smack.services.RetrofitRequests
import br.com.dell.smack.services.UserDataService
import br.com.dell.smack.ui.dialogs.DialogFragmentAddChannel
import br.com.dell.smack.utilities.SOCKET_URL
import br.com.dell.smack.utilities.Utils
import com.github.nkzawa.emitter.Emitter
import com.github.nkzawa.socketio.client.IO
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.nav_header_main.view.*




class MainActivity : AppCompatActivity() {

    private var isChannelCreated: Boolean = false
    private var isMessageSent: Boolean = false
    private val socket = IO.socket(SOCKET_URL)
    private var channelAdapter: ChannelsRecyclerViewAdapter? = null
    private var messageAdapter: MessageListAdapter? = null
    private var channels = MessageService.channels
    private var selectedChannel: Channel? = null
    private val MESSAGES_DOWNLOADED = "DOWNLOADED MESSAGES"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        socket.on("channelCreated", onNewChannel)
        socket.on("messageCreated", onNewMessage)
        socket.connect()

        if (intent.hasExtra("LoginData")) {
            setUserDataInNavHeader()
            drawer_layout.openDrawer(GravityCompat.START)
        }

        if (App.prefs!!.isLoggedIn) {
            getSelectedChannelIfExists()
            nav_drawer_header_include.addChannelButton.visibility = View.VISIBLE
        }

        object : ActionBarDrawerToggle(
            this@MainActivity, drawer_layout, toolbar,
            R.string.open_nav_drawer, R.string.close_nav_drawer) {
            override fun onDrawerOpened(drawerView: View) {
                setUpChannelsList()
                super.onDrawerOpened(drawerView)
            }
        }.apply {
            drawer_layout.addDrawerListener(this)
            isDrawerIndicatorEnabled = true
            syncState()
        }

        setUpMessageList()
        setListeners()
    }

    private fun setUpMessageList() {
        this.messageAdapter =
            MessageListAdapter(this@MainActivity, layoutInflater, MessageService.messages)
        message_recycler_view.apply {
            adapter = this@MainActivity.messageAdapter
            layoutManager = LinearLayoutManager(this@MainActivity)
        }
    }

    private fun getSelectedChannelIfExists() {
        if (MessageService.channels.count() > 0) {
            selectedChannel = MessageService.channels[0]
            setUpChannelsList()
            setUserDataInNavHeader()
            updateWithChannel()
        }
    }

    private fun updateWithChannel() {
        val channelTemplate = "#${selectedChannel!!.name}"
        mainChannelName.text = channelTemplate

        downloadMessagesForChannel()
    }

    private fun downloadMessagesForChannel() {
        if (this.selectedChannel != null) {
            RetrofitRequests.getMessagesFromChannel(this@MainActivity,
                                                    this.selectedChannel!!.id) { message, requestSuccess ->
                if (requestSuccess) {
                    scrollToTheBottomOfListMessage()
                    Log.d(MESSAGES_DOWNLOADED, message)
                } else {
                    Toast.makeText(this@MainActivity, message, Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun scrollToTheBottomOfListMessage() {
        messageAdapter!!.let {
            it.notifyDataSetChanged()
            if (it.itemCount > 0) {
                message_recycler_view.smoothScrollToPosition(it.itemCount - 1)
            }
        }
    }

    private fun setUserDataInNavHeader() {
        if (App.prefs!!.isLoggedIn) {
            nav_view.nav_drawer_header_include.userImageNavHeader.apply {
                this.setImageResource(resources.getIdentifier(UserDataService.userAvatarName,"drawable", packageName))
                this.setBackgroundColor(Utils.returnAvatarColor(UserDataService.userAvatarColor))
            }

            nav_view.nav_drawer_header_include.userNameNavHeader.text = UserDataService.userName
            nav_view.nav_drawer_header_include.userEmailNavHeader.text = App.prefs!!.userEmail
            nav_view.nav_drawer_header_include.loginButtonNavHeader.text =
                getString(R.string.logout)
        }
    }

    private fun setUpChannelsList() {
        this.channelAdapter =
            ChannelsRecyclerViewAdapter(channels, layoutInflater) { channelPosition ->
                selectedChannel = MessageService.channels[channelPosition]
                drawer_layout.closeDrawer(GravityCompat.START)
                updateWithChannel()
            }

        channel_recycler_view.apply {
            adapter = this@MainActivity.channelAdapter
            layoutManager = LinearLayoutManager(this@MainActivity)
        }.adapter!!.notifyDataSetChanged()
    }

    private fun setListeners() {
        nav_drawer_header_include.loginButtonNavHeader.setOnClickListener { loginButtonNavClicked() }
        nav_drawer_header_include.addChannelButton.setOnClickListener { addChannelClicked() }
        sendMessageButton.setOnClickListener { sendMessageButtonClicked() }
    }

    private fun loginButtonNavClicked() {
        if (App.prefs!!.isLoggedIn) {
            logUserOut()
            return
        }

        startActivity(Intent(this@MainActivity, LoginActivity::class.java))
    }

    private fun logUserOut() {
        UserDataService.logout()
        channelAdapter?.notifyDataSetChanged()
        messageAdapter?.notifyDataSetChanged()

        drawer_layout.isDrawerOpen(GravityCompat.START)
        mainChannelName.text = getString(R.string.please_log_in)
        nav_view.nav_drawer_header_include.userEmailNavHeader.text = ""
        nav_view.nav_drawer_header_include.userNameNavHeader.text = ""
        nav_view.nav_drawer_header_include.loginButtonNavHeader.text = getString(R.string.log_in)
        nav_view.nav_drawer_header_include.userImageNavHeader.apply {
            this.setImageDrawable(ContextCompat.getDrawable(this@MainActivity, R.drawable.profiledefault))
            this.setBackgroundColor(Color.TRANSPARENT)
        }
    }

    private fun addChannelClicked() {
        this.isChannelCreated = false //FIX THAT AVOIDS THE EVENT newChannel FIRING MULTIPLE TIMES

        if (App.prefs!!.isLoggedIn) {
            DialogFragmentAddChannel { channelName, channelDesc ->
                if (!socket.connected()) {
                    socket.on("channelCreated", onNewChannel)
                    socket.connect()
                }

                socket.emit("newChannel", channelName, channelDesc)
                this.isChannelCreated = true
            }.show(supportFragmentManager, "DialogFragmentAddChannel")
        }
    }

    private fun sendMessageButtonClicked() {
        this.isMessageSent = false //FIX THAT AVOIDS THE EVENT newMessage FIRING MULTIPLE TIMES

        if (App.prefs!!.isLoggedIn && messageTextField.text.isNotEmpty() && selectedChannel != null) {
            socket.emit(
                "newMessage",
                messageTextField.text.toString(),
                UserDataService.id,
                selectedChannel!!.id,
                UserDataService.userName,
                UserDataService.userAvatarName,
                UserDataService.userAvatarColor
            )
            this.isMessageSent = true
            messageTextField.text.clear()
            hideKeyboard()
        }
    }

    private val onNewChannel = Emitter.Listener { args ->
        when {
            !this.isChannelCreated -> return@Listener
            App.prefs!!.isLoggedIn -> {
                runOnUiThread {
                    val newChannel = Channel(
                        name = args[0] as String,
                        description = args[1] as String,
                        id = args[2] as String
                    )
                    channels.add(newChannel)
                    setUpChannelsList()
                    this.isChannelCreated = false
                }
            }
        }
    }

    private val onNewMessage = Emitter.Listener { args ->

        when {
            !this.isMessageSent -> return@Listener
            App.prefs!!.isLoggedIn -> checkIfMessageComesFromSelectedChannel(args)
        }
    }

    private fun checkIfMessageComesFromSelectedChannel(args: Array<Any>) {
        runOnUiThread {
            if (args[2] as String == this.selectedChannel!!.id) {
                val newMessage = Message(message = args[0] as String,
                                         userName = args[3] as String,
                                         channelId = args[2] as String,
                                         userAvatar = args[4] as String,
                                         userAvatarColor = args[5] as String,
                                         id = args[6] as String,
                                         timeStamp = args[7] as String)

                MessageService.messages.add(newMessage)
                scrollToTheBottomOfListMessage()
                this.isMessageSent = false
            }
        }
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.isDrawerOpen(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    private fun hideKeyboard() {
        val inputMethodManager =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.let {
            if (it.isAcceptingText) {
                it.hideSoftInputFromWindow(currentFocus?.windowToken, 0)
            }
        }
    }

    override fun onDestroy() {
        socket.disconnect()
        socket.off("onNewChannel", onNewChannel)
        socket.off("onNewMessage", onNewMessage)
        super.onDestroy()
    }
}