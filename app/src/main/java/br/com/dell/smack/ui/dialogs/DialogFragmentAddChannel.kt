package br.com.dell.smack.ui.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import br.com.dell.smack.R
import kotlinx.android.synthetic.main.add_channel_dialog_fragment.*
import kotlinx.android.synthetic.main.add_channel_dialog_fragment.view.*


class DialogFragmentAddChannel(private var listener: (String, String) -> Unit) : DialogFragment() {


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.add_channel_dialog_fragment, container, false)
        dialog.window.apply {
            if (this != null) {
                setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                requestFeature(Window.FEATURE_NO_TITLE)
            }
        }
        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.btChannelDialogFragmentAdd.setOnClickListener {
            getChannelName()
        }

        view.btChannelDialogFragmentCancel.setOnClickListener {
            this.dismiss()
        }
    }

    private fun getChannelName() {
        if (edtAddChannelName.text.isEmpty()) {
            tilAddChannelName.error = "Channel name is required!"
            return
        }

        this.dismiss()
        listener(edtAddChannelName.text.toString(), edtAddChannelDesc.text.toString())
    }

}