package br.com.dell.smack.network.model.response

data class MessageFromChannelModelResponse(val _id:String,
                                           val messageBody: String,
                                           val userId: String,
                                           val channelId: String,
                                           val userName: String,
                                           val userAvatar: String,
                                           val userAvatarColor: String,
                                           val __v: String,
                                           val timeStamp: String)